"""net_dcim URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLConf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from django_robohash.views import robohash
from rest_framework import routers
from rest_framework.schemas import get_schema_view

from api.v1 import DataCenterViewSet, RackViewSet, SelfUserView


router = routers.DefaultRouter()
router.register(r'data_centers', DataCenterViewSet)
router.register(r'racks', RackViewSet, basename="Rack")

admin.site.site_header = "NetDCIM Administration"
admin.site.index_title = "Features area"
admin.site.site_title = "NetDCIM Administration"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(router.urls)),
    path('api/v1/self', SelfUserView.as_view()),
    path('api/v1/auth/', include('rest_framework.urls', namespace='api_v1')),
    path('openapi', get_schema_view(title="NetDCIM API DOC", description="API for communicate form frontend …",
                                    version="1.0.0"),
         name='openapi-schema'),
    path('api/v1/avatars/<string>/', robohash, name='robohash'),
    path('swagger-ui/', TemplateView.as_view(template_name='swagger-ui.html',
                                             extra_context={'schema_url': 'openapi-schema'}), name='swagger-ui'),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
