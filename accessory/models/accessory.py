from django.db import models

from accessory.models.choices import AccessoryType
from utils.mixins import NamedModelMixin


class Accessory(NamedModelMixin):
    type = models.PositiveIntegerField(choices=AccessoryType(), default=AccessoryType.faker.id)

    class Meta:
        verbose_name_plural = "Accessories"

    def get_type_desc(self) -> str:
        return AccessoryType.name_from_id(self.type)

    @property
    def type_description(self) -> str:
        return self.get_type_desc()

    def __str__(self) -> str:
        return "{} ({})".format(self.name, self.get_type_desc())
