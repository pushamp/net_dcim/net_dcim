from dj.choices import Choices


class AccessoryType(Choices):
    _ = Choices.Choice

    patch_panel = _("patch panel")
    kvm = _("kvm")
    faker = _("faker")
    ups = _("ups")
