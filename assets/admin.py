from django.contrib import admin

from .models import Manufacturer, AssetModel, Category, Asset


@admin.register(Manufacturer)
class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "manufacturer_kind_str")
    search_fields = ('name',)


@admin.register(AssetModel)
class AssetModelAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'type',
        'manufacturer',
        'power_consumption',
        'height_of_device',
    )
    list_filter = ('manufacturer',)
    search_fields = ('name',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'parent',
        'default_depreciation_rate',
        'lft',
        'rght',
        'tree_id',
        'level',
    )
    list_filter = ('parent',)
    search_fields = ('name',)


@admin.register(Asset)
class AssetAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'price_currency',
        'price',
        'model',
        'hostname',
        'sn',
        'barcode',
        'inventory_number',
        'task_url',
    )
    list_filter = ('model',)
