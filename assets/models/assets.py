from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from assets.models.choices import ObjectModelType, ManufacturerKind
from utils.mixins import NamedModelMixin, PriceMixin, NullableCharField, ChoiceArrayField


def _default_kinds():
    return [ManufacturerKind.hardware.id]


class Manufacturer(NamedModelMixin, models.Model):
    name = models.CharField(verbose_name=_('Verbose name'), null=False, blank=False, max_length=64)
    manufactured_kind = ChoiceArrayField(models.PositiveIntegerField(choices=ManufacturerKind()),
                                         verbose_name=_("Manufacturer Kind"),
                                         null=False,
                                         blank=False,
                                         default=_default_kinds, size=2)

    def __str__(self):
        return "{} ({})".format(self.name, self.manufacturer_kind_str())

    def manufacturer_kind_str(self):
        return ", ".join([ManufacturerKind.name_from_id(i) for i in self.manufactured_kind])


class AssetModel(NamedModelMixin, models.Model):
    type = models.PositiveIntegerField(verbose_name=_('type'), choices=ObjectModelType(), )
    manufacturer = models.ForeignKey(Manufacturer, to_field='id', on_delete=models.PROTECT, blank=True, null=True)
    category = TreeForeignKey('Category', null=False, related_name='models', on_delete=models.CASCADE)
    power_consumption = models.PositiveIntegerField(verbose_name=_("Power consumption"), default=0)
    height_of_device = models.FloatField(verbose_name=_("Height of device"), default=0,
                                         validators=[MinValueValidator(0)])

    class Meta:
        verbose_name = _('model')
        verbose_name_plural = _('models')

    def __str__(self):
        if self.category_id:
            return '[{}] {} {}'.format(self.category, self.manufacturer, self.name)
        else:
            return '{} {}'.format(self.manufacturer, self.name)


class Category(MPTTModel, NamedModelMixin, models.Model):
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True,
                            on_delete=models.CASCADE)
    default_depreciation_rate = models.DecimalField(blank=True, decimal_places=2,
                                                    default=settings.DEFAULT_DEPRECIATION_RATE,
                                                    help_text=_(
                                                        'This value is in percentage.'
                                                        ' For example value: "100" means it depreciates during a year.'
                                                        ' Value: "25" means it depreciates during 4 years, and so on...'
                                                    ),
                                                    max_digits=5,
                                                    )

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.name

    def get_default_depreciation_rate(self, category=None):
        if category is None:
            category = self

        if category.default_depreciation_rate:
            return category.default_depreciation_rate
        elif category.parent:
            return self.get_default_depreciation_rate(category.parent)
        return 0


class Asset(PriceMixin, models.Model):
    model = models.ForeignKey(AssetModel, to_field='id', related_name='assets', on_delete=models.PROTECT)
    hostname = NullableCharField(blank=True, default=None, max_length=255, null=True, verbose_name=_('hostname'),
                                 unique=True)
    sn = NullableCharField(blank=True, max_length=200, null=True, verbose_name=_('SN'), unique=True)
    barcode = NullableCharField(blank=True, default=None, max_length=200, null=True, unique=True,
                                verbose_name=_('barcode'))
    inventory_number = NullableCharField(blank=True, default=None, max_length=200, null=True,
                                         verbose_name=_('inventory number'))
    task_url = models.URLField(blank=True, help_text=_('External workflow system URL'), max_length=2048, null=True)

    def __str__(self):
        return self.hostname or ''

    def clean(self):
        if not self.sn and not self.barcode:
            error_message = [_('SN or BARCODE field is required')]
            raise ValidationError({'sn': error_message, 'barcode': error_message})

    def save(self, *args, **kwargs):
        # if we save barcode as empty string (instead of None) we could have only one asset with empty barcode
        # (because of `unique` constraint), and if we save barcode as None you could have many assets with empty barcode
        # (because `unique` constraint is skipped)
        for unique_field in ['barcode', 'sn']:
            value = getattr(self, unique_field, None)
            if value == '':
                value = None
            setattr(self, unique_field, value)
        return super(Asset, self).save(*args, **kwargs)
