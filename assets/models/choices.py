from dj.choices import Choices


class ObjectModelType(Choices):
    _ = Choices.Choice

    back_office = _('back office')
    data_center = _('data center')


class ManufacturerKind(Choices):
    _ = Choices.Choice

    software = _('Software')
    hardware = _('Hardware')
