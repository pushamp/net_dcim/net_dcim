import logging
import re
import uuid

import django.core.validators
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import PositiveIntegerField
from django.utils.translation import ugettext_lazy as _

from accessory.models.accessory import Accessory
from assets.models.assets import Asset
from datacenters.models import Orientation, RackOrientation
from datacenters.models.choices import DataCenterAssetStatus
from utils.mixins import NamedModelMixin, UUIDModel

logger = logging.getLogger(__name__)

# i.e. number in range 1-16 and optional postfix 'A' or 'B'
VALID_SLOT_NUMBER_FORMAT = re.compile('^([1-9][A,B]?|1[0-6][A,B]?)$')

ACCESSORY_DATA = [
    'brush', 'patch_panel_fc', 'patch_panel_utp', 'organizer', 'power_socket'
]


class DataCenter(NamedModelMixin):
    location = models.CharField(verbose_name='Location of data center', null=False, blank=False, max_length=256)

    @property
    def rack_set(self):
        return Rack.objects.select_related('server_room').filter(server_room__data_center=self)

    @property
    def server_rooms(self):
        return ServerRoom.objects.filter(data_center=self)

    @property
    def total_units(self) -> int:
        return sum(map(lambda x: x.max_u_height, self.rack_set))

    @property
    def total_free_units(self) -> int:
        return sum(map(lambda x: x.get_free_units(), self.rack_set))

    def capacity(self) -> int:
        if self.total_free_units <= 0:
            return 0
        return round(100/(self.total_units/self.total_free_units))

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class ServerRoomManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('data_center')


class ServerRoom(NamedModelMixin):
    data_center = models.ForeignKey(DataCenter, to_field='id', verbose_name=_("data center"), on_delete=models.CASCADE)
    data_center._autocomplete = True
    data_center._filter_title = _('data center')
    visualization_cols_num = models.PositiveIntegerField(verbose_name=_('visualization grid columns number'),
                                                         default=20,
                                                         validators=[
                                                             django.core.validators.MinValueValidator(1),
                                                             django.core.validators.MaxValueValidator(100)])
    visualization_rows_num = models.PositiveIntegerField(verbose_name=_('visualization grid rows number'), default=20,
                                                         validators=[django.core.validators.MinValueValidator(1),
                                                                     django.core.validators.MaxValueValidator(100)]
                                                         )

    class Meta:
        unique_together = ('name', 'data_center')

    objects = ServerRoomManager()

    @property
    def rack_set(self):
        return Rack.objects.select_related('server_room').filter(server_room=self)

    def __str__(self):
        return '{} ({})'.format(self.name, self.data_center.name)


class RackAccessory(UUIDModel):
    accessory = models.ForeignKey(Accessory, to_field='id', on_delete=models.CASCADE)
    rack = models.ForeignKey('Rack',to_field='id',  on_delete=models.CASCADE)
    position = models.PositiveIntegerField(null=True)
    orientation = models.PositiveIntegerField(choices=Orientation(), default=Orientation.front.id)
    remarks = models.CharField(verbose_name=_("Additional remarks"), max_length=1024, blank=True)

    class Meta:
        verbose_name_plural = _("rack accessories")

    def get_orientation_desc(self):
        return Orientation.name_from_id(self.orientation)

    def __str__(self):
        rack_name = self.rack.name if self.rack else ""
        accessory_name = self.accessory.name if self.accessory else ""
        return "{rack_name} - {accessory_name}".format(rack_name=rack_name, accessory_name=accessory_name)


class Rack(NamedModelMixin):
    server_room = models.ForeignKey(ServerRoom, to_field='id', verbose_name=_('server room'), null=True, blank=False,
                                    on_delete=models.CASCADE)
    server_room._autocomplete = True
    server_room._filter_title = _('server room')
    max_u_height = models.IntegerField(default=48)

    orientation = models.PositiveIntegerField(choices=RackOrientation(), default=RackOrientation.top.id)

    visualization_col = models.PositiveIntegerField(verbose_name=_('column number on visualization grid'), default=0)
    visualization_row = models.PositiveIntegerField(verbose_name=_('row number on visualization grid'), default=0)

    class Meta:
        unique_together = ('name', 'server_room')

    def get_free_units(self):
        u_list = [True] * self.max_u_height
        accessories = RackAccessory.objects.values_list('position').filter(rack=self)
        dc_assets = self.get_root_assets().values_list('position', 'asset__model__height_of_device')

        def fill_units_list(objects, height_of_device=lambda obj: 1):
            for obj in objects:
                # if position is None when objects simply does not have (assigned) position and position 0 is for some
                # accessories (pdu) with left-right orientation and should not be included in free/filled space.
                if obj[0] == 0 or obj[0] is None:
                    continue
                start = obj[0] - 1
                end = min(self.max_u_height, obj[0] + int(height_of_device(obj))) - 1
                height = end - start
                if height:
                    u_list[start:end] = [False] * height

        fill_units_list(accessories)
        fill_units_list(dc_assets, lambda obj: obj[1])
        return sum(u_list)

    def get_orientation_desc(self):
        return RackOrientation.name_from_id(self.orientation)

    def get_root_assets(self, side=None):
        filter_kwargs = {'rack': self}
        if side:
            filter_kwargs['orientation'] = side
        else:
            filter_kwargs['orientation__in'] = [Orientation.front, Orientation.back]
        return DataCenterAsset.objects.select_related('rack', 'asset').filter(**filter_kwargs)

    @property
    def root_assets(self):
        return self.get_root_assets()

    @property
    def rack_accessory(self):
        return RackAccessory.objects.filter(rack=self)

    def __str__(self):
        if self.server_room:
            return "{} ({}/{})".format(self.name, self.server_room.data_center, self.server_room.name)
        return self.name


class DataCenterAsset(UUIDModel):
    asset = models.OneToOneField(Asset, to_field='id', null=True, blank=False, on_delete=models.CASCADE)
    rack = models.ForeignKey(Rack, to_field='id', null=True, blank=False, on_delete=models.PROTECT)
    status = PositiveIntegerField(default=DataCenterAssetStatus.new.id, choices=DataCenterAssetStatus())
    position = models.IntegerField(null=True, blank=True)
    orientation = models.PositiveIntegerField(choices=Orientation(), default=Orientation.front.id)

    class Meta:
        verbose_name = _('data center asset')
        verbose_name_plural = _('data center assets')

    def __str__(self):
        return '{} (BC: {} / SN: {})'.format(self.asset.hostname or '-', self.asset.barcode or '-', self.asset.sn or '-')

    def __repr__(self):
        return '<DataCenterAsset: {}>'.format(self.id)

    def get_orientation_desc(self):
        return Orientation.name_from_id(self.orientation)

    def get_location(self):
        location = []
        if self.rack:
            location.extend([self.rack.server_room.data_center.name, self.rack.server_room.name, self.rack.name])
        if self.position:
            location.append(str(self.position))
        return location

    def _validate_orientation(self):
        """
        Validate if orientation is valid for given position.
        """
        if self.position is None:
            return
        if self.position == 0 and not Orientation.is_width(self.orientation):
            msg = 'Valid orientations for picked position are: {}' \
                .format(', '.join(choice.desc for choice in Orientation.WIDTH.choices))
            raise ValidationError({'orientation': [msg]})
        if self.position > 0 and not Orientation.is_depth(self.orientation):
            msg = 'Valid orientations for picked position are: {}' \
                .format(', '.join(choice.desc for choice in Orientation.DEPTH.choices))
            raise ValidationError({'orientation': [msg]})

    def _validate_position(self):
        """
        Validate if position not empty when rack requires it.
        """
        if self.rack and self.position is None:
            msg = 'Position is required for this rack'
            raise ValidationError({'position': [msg]})

    def _validate_position_in_rack(self):
        """
        Validate if position is in rack height range.
        """
        if self.rack and self.position is not None and self.position > self.rack.max_u_height:
            msg = 'Position is higher than "max u height" = {}'.format(self.rack.max_u_height)
            raise ValidationError({'position': [msg]})
        if self.position is not None and self.position < 0:
            msg = 'Position should be 0 or greater'
            raise ValidationError({'position': msg})

    def _validate_position_in_rack_1(self):
        """
        Validate
        """
        if self.rack and self.position is not None:
            self

    def clean(self):
        errors = {}
        for validator in [super().clean, self._validate_orientation, self._validate_position,
                          self._validate_position_in_rack]:
            try:
                validator()
            except ValidationError as e:
                e.update_error_dict(errors)
        if errors:
            raise ValidationError(errors)
