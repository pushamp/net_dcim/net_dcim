from django.contrib import admin
from datacenters.models import DataCenter, Rack, ServerRoom, RackAccessory, DataCenterAsset


admin.site.register(DataCenter)
admin.site.register(Rack)
admin.site.register(ServerRoom)
admin.site.register(RackAccessory)
admin.site.register(DataCenterAsset)
