#!/bin/sh
set -e

. /venv/bin/activate
/venv/bin/python manage.py collectstatic --no-input
exec /venv/bin/gunicorn --bind 0.0.0.0:80 -w 4 --forwarded-allow-ips='*' net_dcim.wsgi:application
