import django_filters
from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from api.v1.serializer import DataCenterSerializer, RackDetailSerializer, SelfUserSerializer
from datacenters.models import DataCenter, Rack


class DataCenterViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = DataCenter.objects.all()
    serializer_class = DataCenterSerializer
    lookup_field = 'id'


class RackFilter(django_filters.FilterSet):
    dc_guid = django_filters.UUIDFilter(field_name="server_room__data_center_id", label='dc_guid')
    server_room_guid = django_filters.UUIDFilter(field_name="server_room_id", label='server_room_guid')

    class Meta:
        model = Rack
        fields = ['dc_guid', 'server_room_guid']


class RackViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = RackDetailSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = RackFilter
    queryset = Rack.objects.all()
    lookup_field = 'id'


class SelfUserView(GenericAPIView):
    pagination_class = None
    serializer_class = SelfUserSerializer

    def get_object(self):
        return User.objects.get(pk=self.request.user.pk)

    def get(self, request):
        snippet = self.get_object()
        serializer = self.get_serializer(snippet)
        return Response(serializer.data)
