from django.contrib.auth.models import User
from rest_framework import serializers

from datacenters.models import DataCenter, Rack, ServerRoom, DataCenterAsset, RackAccessory


class RackSerializer(serializers.ModelSerializer):
    free_units = serializers.IntegerField(source='get_free_units')
    orientation_desc = serializers.CharField(source='get_orientation_desc')

    class Meta:
        model = Rack
        fields = ('id', 'free_units', 'name', 'orientation_desc', 'max_u_height')


class RackAccessorySerializer(serializers.ModelSerializer):
    accessory_type = serializers.CharField(source="accessory.type_description")
    orientation = serializers.CharField(source='get_orientation_desc')

    class Meta:
        model = RackAccessory
        fields = ("position", "orientation", "remarks", "accessory_type")


class DataCenterAssetSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    height_of_device = serializers.SerializerMethodField()
    orientation = serializers.CharField(source='get_orientation_desc')
    sn = serializers.SerializerMethodField()
    barcode = serializers.SerializerMethodField()
    inventory_number = serializers.SerializerMethodField()

    class Meta:
        model = DataCenterAsset
        fields = ('id', 'status', 'position', 'orientation', 'name', 'height_of_device', 'sn', 'barcode',
                  'inventory_number')

    def get_name(self, obj):
        return str(obj)

    def get_height_of_device(self, obj):
        return obj.asset.model.height_of_device

    def get_height_of_device(self, obj):
        return obj.asset.model.height_of_device

    def get_sn(self, obj):
        return obj.asset.sn

    def get_barcode(self, obj):
        return obj.asset.barcode

    def get_inventory_number(self, obj):
        return obj.asset.inventory_number


class RackDetailSerializer(serializers.ModelSerializer):
    root_assets = DataCenterAssetSerializer(many=True)
    free_units = serializers.IntegerField(source='get_free_units')
    orientation_desc = serializers.CharField(source='get_orientation_desc')
    accessory = RackAccessorySerializer(many=True, source="rack_accessory")

    class Meta:
        model = Rack
        fields = ('id', 'free_units', 'name', 'orientation_desc', 'max_u_height', 'root_assets', 'accessory')


class ServerRoomSerializer(serializers.ModelSerializer):
    rack_set = RackSerializer(many=True)

    class Meta:
        model = ServerRoom
        fields = ('id', 'name', 'rack_set')


class DataCenterSerializer(serializers.ModelSerializer):
    server_rooms = ServerRoomSerializer(many=True)

    class Meta:
        model = DataCenter
        fields = ('id', 'name', 'location', 'server_rooms', 'capacity', 'total_units', 'total_free_units')


class SelfUserSerializer(serializers.ModelSerializer):
    short_name = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'email', 'short_name', 'full_name']

    def get_full_name(self, obj):
        return obj.get_full_name()

    def get_short_name(self, obj):
        return obj.get_short_name()
