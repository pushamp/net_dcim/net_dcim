import uuid

from django import forms
from django.contrib.postgres.fields import ArrayField
from django.core import exceptions
from django.db import models
from django.utils.translation import ugettext as _
from djmoney.models.fields import MoneyField

from net_dcim.settings import DEFAULT_CURRENCY_CODE


class ChoiceArrayField(ArrayField):
    """
    A postgres ArrayField that supports the choices property.

    Ref. https://gist.github.com/danni/f55c4ce19598b2b345ef.
    """

    def formfield(self, **kwargs):
        defaults = {
            "form_class": forms.MultipleChoiceField,
            "choices": self.base_field.choices,
        }
        defaults.update(kwargs)
        return super(ArrayField, self).formfield(**defaults)

    def to_python(self, value):
        res = super().to_python(value)
        if isinstance(res, list):
            value = [self.base_field.to_python(val) for val in res]
        return value

    def validate(self, value, model_instance):
        if not self.editable:
            # Skip validation for non-editable fields.
            return

        if self.choices is not None and value not in self.empty_values:
            if set(value).issubset({option_key for option_key, _ in self.choices}):
                return
            raise exceptions.ValidationError(
                self.error_messages["invalid_choice"],
                code="invalid_choice",
                params={"value": value},
            )

        if value is None and not self.null:
            raise exceptions.ValidationError(self.error_messages["null"], code="null")

        if not self.blank and value in self.empty_values:
            raise exceptions.ValidationError(self.error_messages["blank"], code="blank")


class NamedModelMixin(models.Model):
    pkid = models.BigAutoField(primary_key=True, editable=False)
    id = models.UUIDField(verbose_name=_("guid"), default=uuid.uuid4, editable=False, unique=True)
    name = models.CharField(verbose_name=_('Verbose name'), null=False, blank=False, max_length=64)

    class Meta:
        abstract = True


class UUIDModel(models.Model):
    pkid = models.BigAutoField(primary_key=True, editable=False)
    id = models.UUIDField(verbose_name=_("guid"), default=uuid.uuid4, editable=False, unique=True)

    class Meta:
        abstract = True


class PriceMixin(models.Model):
    pkid = models.BigAutoField(primary_key=True, editable=False)
    id = models.UUIDField(verbose_name=_("guid"), default=uuid.uuid4, editable=False, unique=True)
    price = MoneyField(max_digits=15, decimal_places=2, null=True, default=0, default_currency=DEFAULT_CURRENCY_CODE)

    class Meta:
        abstract = True


class NullableFormFieldMixin(object):
    def to_python(self, value):
        if value in self.empty_values:
            return None
        return super().to_python(value)


class NullableCharFormField(NullableFormFieldMixin, forms.CharField):
    pass


class NullableCharFieldMixin(object):
    """
    Mixin for char fields and descendants which will replace empty string value ('') by null when saving to the
    database.

    It's especially useful when field is marked as unique and at the same time allows null/blank
    (`models.CharField(unique=True, null=True, blank=True)`)
    """
    _formfield_class = NullableCharFormField

    def get_prep_value(self, value):
        return super().get_prep_value(value) or None

    def formfield(self, **kwargs):
        defaults = {}
        if self._formfield_class:
            defaults['form_class'] = self._formfield_class
        defaults.update(kwargs)
        return super().formfield(**defaults)


class NullableCharField(NullableCharFieldMixin, models.CharField):
    pass
